tclsh
foreach address {
10.1.1.1
10.1.2.1
10.1.3.1
172.16.12.1
172.16.12.2
172.16.23.2
172.16.23.3
FEC0::1:1
FEC0::3:1
FEC0::13:1
FEC0::13:3
} {
ping $address }
exit
