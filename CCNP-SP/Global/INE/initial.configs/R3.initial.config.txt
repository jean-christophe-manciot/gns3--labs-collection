hostname Rack1R3
!
enable password cisco
!
ip subnet-zero
no ip domain-lookup
!
ip classless
!
line con 0
 exec-timeout 0 0
 logging synchronous
 privilege level 15
line aux 0
 exec-timeout 0 0
 privilege level 15
line vty 0 4
 login
 password cisco
